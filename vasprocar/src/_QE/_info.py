# VASProcar Copyright (C) 2023
# GNU GPL-3.0 license

#################################################################################
# Analisando os arquivos scf.out | nscf.in | nscf.out | bands.in | filband_file #
################ Buscando informacoes do Sistema ################################
#################################################################################

# Valor padrão de algumas tags ========================================
energ_tot_all_electron = 0.0
energ_tot = 0.0
Efermi = -1000.0
n_procar = 1  # ??????????????????
lorbit = 10   # ??????????????????
n_orb = 4     # Número de Orbitais
ispin = 1
SO = 1
LNC = 1

#======================================================================

#------------------------------------------
scf_out = open(dir_files + '/scf.out', "r")
#------------------------------------------

for i in range(20000):
    #---------------------------------
    VTemp = scf_out.readline().split()
    #---------------------------------
                          
    if (len(VTemp) == 6 and (VTemp[1] == 'Fermi' and VTemp[2] == 'energy')):
       Efermi = float(VTemp[4])        # Energia de Fermi do sistema

    if (len(VTemp) == 6 and (VTemp[1] == 'total' and VTemp[2] == 'energy')):
       energ_tot = float(VTemp[4])     # Energia total do sistema

    if (len(VTemp) == 6 and (VTemp[1] == 'all-electron' and VTemp[2] == 'energy')):
       energ_tot_all_electron = float(VTemp[4])     # Energia total de todos os elétrons do sistema

#--------------
scf_out.close()
#--------------

if (Efermi == -1000.0):

   #------------------------------------------
   scf_out = open(dir_files + '/scf.out', "r")
   #------------------------------------------

   for i in range(20000):
       #---------------------------------
       VTemp = scf_out.readline().split()
       #---------------------------------
                      
       if (len(VTemp) == 8 and VTemp[0] == 'highest' and VTemp[1] == 'occupied,'):
          Efermi = ((float(VTemp[7]) - float(VTemp[6]))/2) + float(VTemp[6])     # Energia de Fermi do sistema

   #--------------
   scf_out.close()
   #--------------

#======================================================================

#------------------------------------------
nscf_in = open(dir_files + '/nscf.in', "r")
#------------------------------------------

for i in range(10000):
    #--------------------------------
    VTemp = nscf_in.readline()
    VTemp = VTemp.replace('=', ' = ')
    VTemp = VTemp.replace(',', ' , ')
    VTemp = VTemp.replace("'", "")
    VTemp = VTemp.split()
    #--------------------------------
                          
    # if (len(VTemp) > 0 and VTemp[0] == 'nat'):
    #    ni = int(VTemp[2])     # Numero de ions da rede
                          
    # if (len(VTemp) > 0 and VTemp[0] == 'ntyp'):
    #    types = int(VTemp[2])  # Numero de diferentes tipos de ions da rede
                          
    if (len(VTemp) > 0 and VTemp[0] == 'nspin'):
       ispin = int(VTemp[2])    # Variavel que determina se o calculo é spin polarizado ou não
                          
    if (len(VTemp) > 0 and VTemp[0] == 'lspinorb'):
       VTemp[2] = VTemp[2].replace('.', '')
       if (VTemp[2][0] == 't' or VTemp[2][0] == 'T'):
          SO = 2                # Variavel que informa se o cálculo possui acoplamento spin-orbita ou não
          LNC = 2

#--------------
nscf_in.close()
#--------------

#======================================================================

#--------------------------------------------
nscf_out = open(dir_files + '/nscf.out', "r")
#--------------------------------------------
                                
for line in nscf_out:   
    if 'unit-cell' in line: 
       break

VTemp = nscf_out.readline().split()
ni = int(VTemp[4])            # Numero de ions da rede

VTemp = nscf_out.readline().split()
types = int(VTemp[5])         # Numero de diferentes tipos de ions da rede

VTemp = nscf_out.readline().split()
n_eletrons = float(VTemp[4])  # Numero de elétrons

VTemp = nscf_out.readline().split()
nb = int(VTemp[4])   # Numero de bandas
nb = nb*ispin        # ????????????????

#----------------------------------------------------------------------

test = 'null'

while (test != 'celldm(1)='):    
      VTemp = nscf_out.readline().split()
      if (len(VTemp) == 6):
         test = str(VTemp[0]) 

cell_1 = float(VTemp[1]); cell_2 = float(VTemp[3]); cell_3 = float(VTemp[5]) 

VTemp = nscf_out.readline().split()
cell_4 = float(VTemp[1]); cell_5 = float(VTemp[3]); cell_6 = float(VTemp[5])

Parametro = cell_1

VTemp = nscf_out.readline()
VTemp = nscf_out.readline()

A1 = nscf_out.readline().split()
A1x = float(A1[3]); A1y = float(A1[4]); A1z = float(A1[5])  # Leitura das coordenadas (X, Y, Z) do vetor primitivo (A1) da celula unitaria no espaco real

A2 = nscf_out.readline().split()
A2x = float(A2[3]); A2y = float(A2[4]); A2z = float(A2[5])  # Leitura das coordenadas (X, Y, Z) do vetor primitivo (A2) da celula unitaria no espaco real

A3 = nscf_out.readline().split()
A3x = float(A3[3]); A3y = float(A3[4]); A3z = float(A3[5])  # Leitura das coordenadas (X, Y, Z) do vetor primitivo (A3) da celula unitaria no espaco real

#----------------------------------------------------------------------

VTemp = nscf_out.readline()
VTemp = nscf_out.readline()

B1  = nscf_out.readline().split()
B1x = float(B1[3]); B1y = float(B1[4]); B1z = float(B1[5])  # Leitura das coordenadas (Kx, Ky, Kz) do vetor primitivo (B1) da 1º Zona de Brillouin no espaço recíproco

B2  = nscf_out.readline().split()
B2x = float(B2[3]); B2y = float(B2[4]); B2z = float(B2[5])  # Leitura das coordenadas (Kx, Ky, Kz) do vetor primitivo (B2) da 1º Zona de Brillouin no espaço recíproco

B3  = nscf_out.readline().split()
B3x = float(B3[3]); B3y = float(B3[4]); B3z = float(B3[5])  # Leitura das coordenadas (Kx, Ky, Kz) do vetor primitivo (B3) da 1º Zona de Brillouin no espaço recíproco

#----------------------------------------------------------------------    
                            
for line in nscf_out:   
    if 'site' in line: 
       break 

rotulo = [0]*(ni+1)
rotulo_temp = [0]*(ni+1)                              

for i in range(1,(ni+1)):
    VTemp = nscf_out.readline().split()
    rotulo[i] = VTemp[1]  # Obtenção dos rótulos para cada ion da rede

#----------------------------------------------------------------------  

test = 'null'

while (test == 'number'):
      #----------------------------------------------
      VTemp = nscf_out.readline().replace('=', ' = ')
      VTemp = VTemp.split()
      #--------------------
      if (len(VTemp) > 6):
         test = str(VTemp[0])
         nk = int(VTemp[5])

#---------------
nscf_out.close()
#---------------

#======================================================================

#-------------------------------------------
bands = open(dir_files + '/' + filband, "r")
#-------------------------------------------

#--------------------------------
VTemp = bands.readline()
VTemp = VTemp.replace('=', ' = ')
VTemp = VTemp.replace(',', ' , ')
VTemp = VTemp.replace('/', ' / ')
VTemp = VTemp.split()
#--------------------------------

nb = int(VTemp[3])  # Numero de bandas
nb = nb*ispin       # ??????????????????
nk = int(VTemp[7])  # Numero de pontos-k 

#------------
bands.close()
#------------

#======================================================================

#--------------------------------------------------------
inform = open(dir_files + '/output/informacoes.txt', "w")
#--------------------------------------------------------

inform.write(" \n")
inform.write("############################################################## \n")
inform.write(f'# {VASProcar_name} \n')
inform.write(f'# {url_1} \n')
inform.write(f'# {url_2} \n')
inform.write("############################################################## \n")
inform.write(" \n")

inform.write("------------------------------------------------------ \n")

if (SO == 1):
   inform.write("LSORBIT = .FALSE. (Calculo sem acoplamento SO) \n")
if (SO == 2):
   inform.write("LSORBIT = .TRUE. (Calculo com acoplamento SO) \n")

inform.write("------------------------------------------------------ \n")

# if (n_procar == 1): inform.write(f'nº Pontos-k = {nk};  nº Bandas = {nb} \n')
# if (n_procar > 1):  inform.write(f'nº Pontos-k = {nk*n_procar} (nº PROCARs = {n_procar});  nº Bandas = {nb} \n')   

inform.write(f'nº Pontos-k = {nk};  nº Bandas = {nb} \n')
inform.write(f'nº ions = {ni};  nº eletrons = {n_eletrons} \n')

inform.write("----------------------------------------------------- \n")
inform.write(f'ISPIN = {ispin} ')
if (ispin == 1): inform.write("(sem polarizacao de spin) \n")
if (ispin == 4): inform.write("(com polarizacao de spin) \n")
inform.write("----------------------------------------------------- \n")

if (Efermi != -1000.0):
   inform.write(f'Energia de fermi = {Efermi} eV \n')
   inform.write("----------------------------------------------------- \n")

#------------------------------------------------------------------------------
# inform.write(f'Ultima Banda ocupada = {n1_valencia} \n')
# inform.write(f'Primeira Banda vazia = {n1_conducao} \n')
# if (kp1 == kp2):
#    inform.write(f'GAP (direto) = {GAP:.4f} eV  -  Kpoint {kp1} \n')
# if (kp1 != kp2):
# inform.write(f'GAP (indireto) = {GAP:.4f} eV  //  Kpoints {kp1} e {kp2} \n')
# inform.write("---------------------------------------------------- \n")
#------------------------------------------------------------------------------

if (energ_tot_all_electron != 0.0):
   inform.write(f'total all-electron energy = {(energ_tot_all_electron/13.605684958731):.6f} eV ({(energ_tot_all_electron):.6f} Ry) \n')
if (energ_tot != 0.0):
   inform.write(f'total energy = {(energ_tot/13.605684958731):.6f} eV  ({(energ_tot):.6f} Ry) \n')
inform.write("----------------------------------------------------- \n")

# inform.write(" \n")
# inform.write("################# Magnetizacao: ##################### \n")
# inform.write(f'Eixo X:  total = {mag_tot_x:.4f} \n')
# inform.write(f'Eixo Y:  total = {mag_tot_y:.4f} \n')
# inform.write(f'Eixo Z:  total = {mag_tot_z:.4f} \n')
# inform.write("##################################################### \n")

#-----------------------------------------------------------------------

inform.write(" \n")
inform.write("***************************************************** \n")
inform.write("Vetores Primitivos da Rede Cristalina *************** \n")
inform.write(f'A1 = Param.({(A1x):12.9f}, {(A1y):12.9f}, {(A1z):12.9f}) \n')
inform.write(f'A2 = Param.({(A2x):12.9f}, {(A2y):12.9f}, {(A2z):12.9f}) \n')
inform.write(f'A3 = Param.({(A3x):12.9f}, {(A3y):12.9f}, {(A3z):12.9f}) \n')
inform.write(f'Param. = {(Parametro/1.8897259886):.6f} Angs.  ({Parametro} Bohr|a.u.) \n')
inform.write("***************************************************** \n")
inform.write(" \n")

#-----------------------------------------------------------------------

inform.write("***************************************************** \n")
inform.write("Vetores Primitivos da Rede Reciproca **************** \n")
inform.write(f'B1 = 2pi/Param.({(B1x):11.8f}, {(B1y):11.8f}, {(B1z):11.8f}) \n')
inform.write(f'B2 = 2pi/Param.({(B2x):11.8f}, {(B2y):11.8f}, {(B2z):11.8f}) \n')
inform.write(f'B3 = 2pi/Param.({(B3x):11.8f}, {(B3y):11.8f}, {(B3z):11.8f}) \n')
inform.write(f'Param. = {(Parametro/1.8897259886):.6f} Angs.  ({Parametro} Bohr|a.u.) \n')
inform.write("***************************************************** \n")
inform.write(" \n")

#-----------------------------------------------------------------------

#-------------
inform.close()
#-------------
