# VASProcar Copyright (C) 2023
# GNU GPL-3.0 license

def execute_python_file(filename: str):
    return exec(open(main_dir + str(filename)).read(), globals())

#-----------------------------------------------------------------------
# Check whether the folder 'Bands' exists ------------------------------
#-----------------------------------------------------------------------
if os.path.isdir(dir_files + '/output/Bandas'):
   0 == 0
else:
   os.mkdir(dir_files + '/output/Bandas')
#----------------------------------------

#======================================================================
# Getting the input parameters ========================================
#======================================================================
execute_python_file(filename = DFT + '_info.py')

#======================================================================
# Get the input from user =============================================
#======================================================================

print ("##############################################################")
print ("##################### Band Structure Plot ####################")
print ("##############################################################")
print (" ")

if (escolha == -1):

   if (len(inputs) == 0):
      print ("##############################################################")
      print ("Regarding the Plot of Bands, choose: =========================")
      print ("[0] Plot all bands ===========================================")
      print ("[1] Plot selected bands ======================================")
      print ("##############################################################")
      esc_bands = input (" "); esc_bands = int(esc_bands)
      print(" ")

   if (esc_bands == 0):
      bands_range = '1:' + str(nb)

   if (esc_bands == 1):
      if (len(inputs) == 0): 
         print ("##############################################################")
         print ("Choose the bands to be plotted through interval ============= ")
         print ("Type as in the examples below =============================== ")
         print ("------------------------------------------------------------- ")
         print ("Bands can be added in any order ----------------------------- ")
         print ("------------------------------------------------------------- ")
         print ("bands_intervals  35:42                                        ")          
         print ("bands_intervals  1:15 27:69 18:19 76*                         ")
         print ("bands_intervals  7* 9* 11* 13*                                ")
         print ("##############################################################")
         bands_range = input ("bands_intervals  ")
         print (" ")
      #------------------------------------------------------------------------------------------
      selected_bands = bands_range.replace(':', ' ').replace('-', ' ').replace('*', ' *').split()
      loop = int(len(selected_bands)/2)
      #------------------------------------------------------------------------------------------

      for i in range (1,(loop+1)):
          #--------------------------------------------------------
          loop_i = int(selected_bands[(i-1)*2])
          if (selected_bands[((i-1)*2) +1] == "*"):
             selected_bands[((i-1)*2) +1] = selected_bands[(i-1)*2]
          loop_f = int(selected_bands[((i-1)*2) +1])
          #----------------------------------------------------------------------------------------
          if ((loop_i > nb) or (loop_f > nb) or (loop_i < 0) or (loop_f < 0) or (loop_i > loop_f)):
             print (" ")
             print ("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
             print ("ERROR: The values of the informed bands are incorrect %%%%")
             print ("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
             confirmation = input (" ")
             exit()

   if (len(inputs) == 0):
      print ("##############################################################") 
      print ("with respect to energy, would you like? ======================")
      print ("[0] Use the default energy value from DFT output =============")
      print ("[1] Shift the Fermi level to 0.0 eV  =========================")
      print ("##############################################################")
      esc_fermi = input (" "); esc_fermi = int(esc_fermi)
      print (" ")   

   if (len(inputs) == 0):
      print ("##############################################################") 
      print ("Do you want to modify the energy range to be plotted? ========")
      print ("[0] NOT                                                       ")
      print ("[1] YES                                                       ")
      print ("##############################################################")
      esc_range_E = input (" "); esc_range_E = int(esc_range_E)
      print (" ")

   if (esc_range_E == 1):
      if (len(inputs) == 0):
         print ("##############################################################") 
         print ("Enter the energy range to be plotted: ========================")
         print ("Note: Enter the lowest and highest energy value to be plotted ")
         print ("      in relation to the Fermi Level                          ")
         print ("Examples:                                                     ")
         print ("--------------------------------------------------------------")
         print ("E_min E_max: -3.0 15.0                                        ")
         print ("E_min E_max: -5.1 5.0                                         ")
         print ("##############################################################")      
         range_E = input ("E_min E_max:  ")
         print (" ")
      #---------------------------------------------------------------------------------------
      selected_energ = range_E.replace('-', ' -').replace('+', ' +').replace(':', ' ').split()
      #--------------------------------------------------------------------------------------- 
      E_min = float(selected_energ[0])
      E_max = float(selected_energ[1])

   if (len(inputs) == 0):
      print ("##############################################################")
      print ("Would you like to label the k-points?                         ")
      print ("[0] DO NOT label the k-points  ===============================")
      print ("[1] highlight k-points present in KPOINTS file ===============")
      print ("[2] Customize: highlight and Label k-points   ================")
      print ("##############################################################") 
      dest_k = input (" "); dest_k = int(dest_k)
      print (" ")
   #--------------
   l_symmetry = 0
   
   if (DFT == '_QE/' and dest_k == 2 and len(inputs) == 0 ):
      print ("##############################################################")
      print ("Do you want to insert symmetries as k-point label?            ")
      print ("[0] NOT                                                       ")
      print ("[1] YES                                                       ")
      print ("##############################################################") 
      l_symmetry = input (" "); l_symmetry = int(l_symmetry)
      print (" ")       

   if (dest_k == 2):
      print ("##############################################################")
      print ("label.TXT file should be found inside the 'output' folder ====")
      print ("after reading the PROCAR file ================================")
      print ("##############################################################") 
      print (" ")
      #-----------
      Dimensao = 1

   if (dest_k != 2 and len(inputs) == 0):
      print ("##############################################################")
      print ("Would you like to choose k-axis units?                        ")
      print ("[1] 2pi/Param. (Param. in Angs.) =============================")
      print ("[2] 1/Angs. ==================================================")
      print ("[3] 1/nm.   ==================================================")
      print ("##############################################################") 
      Dimensao = input (" "); Dimensao = int(Dimensao)
      print (" ")      

if (escolha == 1):
   bands_range = '1:' + str(nb)
   esc_fermi = 1
   esc_range_E = 0
   Dimensao = 1
   dest_k = 1
   l_symmetry = 0

#======================================================================
# Obtaining the results from DFT outpout files ========================
#======================================================================

execute_python_file(filename = DFT + '_nscf.py')
  
#----------------------
if (Efermi == -1000.0):
   Efermi = 0.0
   esc_fermi = 0 

if (esc_fermi == 0):
   dE_fermi = 0.0
   dest_fermi = Efermi

if (esc_fermi == 1):
   dE_fermi = (Efermi)*(-1)
   dest_fermi = 0.0

if (esc_range_E == 0):
   E_min = energ_min - Efermi
   E_max = energ_max - Efermi
#----------------------------

#======================================================================
# Getting k-points / labels ===========================================
#======================================================================
execute_python_file(filename = DFT + '_label.py')

#======================================================================
# Copy Bandas.dat to the output folder directory ======================
#======================================================================

try: f = open(dir_files + '/output/Bandas/Bandas.dat'); f.close(); os.remove(dir_files + '/output/Bandas/Bandas.dat')
except: 0 == 0
  
source = dir_files + '/output/Bandas.dat'
destination = dir_files + '/output/Bandas/Bandas.dat'
shutil.copyfile(source, destination)

os.remove(dir_files + '/output/Bandas.dat')

#======================================================================
#======================================================================
# Band Structure Plot using (GRACE) ===================================
#====================================================================== 
#======================================================================

if (save_agr == 1):

   print(" ")
   print ("================= Plotting the bands (Grace): =================")

   execute_python_file(filename = 'plot/Grace/plot_bandas_2D.py')

   print ("---------- Bands Plot via Grace (.agr file) completed ---------")

#======================================================================
#======================================================================
# Band Structure Plot using (Matplotlib) ==============================
#====================================================================== 
#======================================================================

#----------------------------------------------------------------------
# Copy Bandas.py to the output folder directory  ----------------------
#----------------------------------------------------------------------

try: f = open(dir_files + '/output/Bandas/Bandas.py'); f.close(); os.remove(dir_files + '/output/Bandas/Bandas.py')
except: 0 == 0
  
source = main_dir + '/plot/plot_bandas_2D.py'
destination = dir_files + '/output/Bandas/Bandas.py'
shutil.copyfile(source, destination)

#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
# Allowing Bandas.py to be executed separatedly ---------------------------------------
#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------

file = open(dir_files + '/output/Bandas/Bandas.py', 'r')
lines = file.readlines()
file.close()

linha = 4

lines.insert(linha, '\n')
linha += 1; lines.insert(linha, '###################################################################### \n')
linha += 1; lines.insert(linha, f'# {VASProcar_name} Copyright (C) 2023 \n')
linha += 1; lines.insert(linha, f'# GNU GPL-3.0 license \n')
linha += 1; lines.insert(linha, f'# {url_1} \n')
linha += 1; lines.insert(linha, f'# {url_2} \n')
linha += 1; lines.insert(linha, f'# {url_3} \n')
linha += 1; lines.insert(linha, '###################################################################### \n')
linha += 1; lines.insert(linha, '# Authors:                                                             \n')
linha += 1; lines.insert(linha, '# ==================================================================== \n')
linha += 1; lines.insert(linha, '# Augusto de Lelis Araujo                                              \n')
linha += 1; lines.insert(linha, '# [2022-2023] CNPEM|Ilum|LNNano (Campinas-SP/Brazil)                   \n')
linha += 1; lines.insert(linha, '# [2007-2022] Federal University of Uberlandia (Uberlândia-MG/Brazil)  \n')
linha += 1; lines.insert(linha, '# e-mail: augusto-lelis@outlook.com                                    \n')
linha += 1; lines.insert(linha, '# ==================================================================== \n')
linha += 1; lines.insert(linha, '# Renan da Paixao Maciel                                               \n')
linha += 1; lines.insert(linha, '# Uppsala University (Uppsala/Sweden)                                  \n')
linha += 1; lines.insert(linha, '# e-mail: renan.maciel@physics.uu.se                                   \n')
linha += 1; lines.insert(linha, '###################################################################### \n')
linha += 1; lines.insert(linha, '\n')

linha += 1; lines.insert(linha, '#===================================================================== \n')
linha += 1; lines.insert(linha, '# These are the parameters that allows the code to run separatedly === \n')
linha += 1; lines.insert(linha, '#===================================================================== \n')
linha += 1; lines.insert(linha, '\n')
linha += 1; lines.insert(linha, f'n_procar = {n_procar}  #  Total Num. of PROCAR files \n')
linha += 1; lines.insert(linha, f'nk = {nk}              #  Total Num. of k-points \n')
linha += 1; lines.insert(linha, f'nb = {nb}              #  Total Num. of bands \n')
linha += 1; lines.insert(linha, f'bands_range = "{bands_range}"  # Bands to be plotted \n')
linha += 1; lines.insert(linha, f'ispin = {ispin}        # [1] Calculation without Spin polarization; [2] Calculation with Spin polarization \n')
linha += 1; lines.insert(linha, f'E_min = {E_min}        #  Lower energy value of the bands in the plot (in relation to the Fermi level) \n')
linha += 1; lines.insert(linha, f'E_max = {E_max}        #  Higher energy value of the bands in the plot (in relation to the Fermi level) \n')
linha += 1; lines.insert(linha, f'Efermi = {Efermi}      #  Fermi energy from DFT outpout file \n')
linha += 1; lines.insert(linha, f'esc_fermi = {esc_fermi}  #  Would you like to shift the Fermi level? [0] No, use the value obtained from VASP [1] Yes, shift the Fermi level to 0.0 eV \n')
linha += 1; lines.insert(linha, f'Dimensao = {Dimensao}    #  [1] (kx,ky,kz) in 2pi/Param.; [2] (kx,ky,kz) in 1/Angs.; [3] (kx,ky,kz) in 1/nm.; [4] (k1,k2,k3) \n')
linha += 1; lines.insert(linha, f'dest_k = {dest_k}        #  [0] DO NOT label the k-points; [1] highlight k-points present in KPOINTS file; [2] Customize: highlight and Label k-points \n')
linha += 1; lines.insert(linha, f'dest_pk = {dest_pk}      #  K-points coordinates to be highlighted in the band structure \n')

if (dest_k != 2):
   label_pk = ['null']*len(dest_pk) 
#-------------------------------------------------------------------------------
if (dest_k == 2): 
   for i in range(contador2):
       for j in range(34):
           if (label_pk[i] == '#' + str(j+1)):
              label_pk[i] = r_matplot[j]
       if (DFT == '_QE/' and l_symmetry == 1):
          label_pk[i] = label_pk[i] + '$_{(' + symmetry_pk[i] + ')}$' 
#-------------------------------------------------------------------------------
linha += 1; lines.insert(linha, f'label_pk  = {label_pk}  #  K-points label \n')
#-------------------------------------------------------------------------------

if (sum_save == 0): save_png = 1
linha += 1; lines.insert(linha, f'save_png = {save_png}; save_pdf = {save_pdf}; save_svg = {save_svg}; save_eps = {save_eps}  #  Plotting output format, where [0] = NOT and [1] = YES \n')
linha += 1; lines.insert(linha, '\n')
linha += 1; lines.insert(linha, '#===================================================================== \n')

file = open(dir_files + '/output/Bandas/Bandas.py', 'w')
file.writelines(lines)
file.close()

#-----------------------------------------------------------
if (sum_save != 0):
   exec(open(dir_files + '/output/Bandas/Bandas.py').read())
#-----------------------------------------------------------

#=======================================================================

print(" ")
print("===========================================================")
print("= Edit the Plot of the bands using the following files ====")
print("= Bandas.py or Bandas.agr (via Grace) generated in the ====")   
print("= output/Bandas folder ====================================") 
print("===========================================================")

#-----------------------------------------------------------------
print(" ")
print("======================= Completed =======================")
print(" ")
#-----------------------------------------------------------------

#=======================================================================
# User option to perform another calculation or finished the code ======
#=======================================================================
if (len(inputs) == 0):
   execute_python_file(filename = '_loop.py')
